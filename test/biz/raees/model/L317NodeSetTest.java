package biz.raees.model;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * The JUnit test for NodeSet
 *
 * @author raees
 *
 */
public class L317NodeSetTest {

	/**
	 * The set of nodes
	 */
	private Set<L317Node> nodes;
	/**
	 * The first test node
	 */
	private L317Node node1;
	/**
	 * The second test node
	 */
	private L317Node node2;
	/**
	 * The third test node
	 */
	private L317Node node3;

	@Before
	public void init() {
		node1 = new L317Node();
		node2 = new L317Node();
		node3 = new L317Node();

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNodeTwice() {
		nodes = new L317NodeSet<L317Node>(2);
		nodes.add(node1);
		nodes.add(node1);
	}

	@Test(expected = NullPointerException.class)
	public void testAddNull() {
		nodes = new L317NodeSet<L317Node>(1);
		nodes.add(null);

	}

	@Test
	public void testAddUnderCapacity() {
		nodes = new L317NodeSet<L317Node>(1);
		final boolean added1 = nodes.add(node1);
		final boolean added2 = nodes.add(node2);
		Assert.assertFalse(added1 && added2);
	}

	@Test
	public void testAddWithThreeNodes() {
		nodes = new L317NodeSet<L317Node>(3);
		final boolean added1 = nodes.add(node1);
		final boolean added2 = nodes.add(node2);
		final boolean added3 = nodes.add(node3);
		Assert.assertTrue(added1 && added2 && added3);
	}

	@Test
	public void testRemoveNode() {
		nodes = new L317NodeSet<L317Node>(2);
		nodes.add(node1);
		nodes.add(node2);
		Assert.assertTrue(nodes.remove(node1));

	}

	@Test
	public void testRemoveNull() {
		nodes = new L317NodeSet<L317Node>(2);
		Assert.assertFalse(nodes.remove(null));
	}

	@Test
	public void testSizeWithTwoNodes() {
		nodes = new L317NodeSet<L317Node>(2);
		nodes.add(node1);
		nodes.add(node2);
		final int size = 2;
		Assert.assertEquals(nodes.size(), size);
	}

}
