package biz.raees.model;

import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test for the NodeSetIterator.
 *
 * @author raees
 *
 */
public class L317NodeSetIteratorTest {

	/**
	 * An iterator of nodes
	 */
	private Iterator<L317Node> $it;

	/**
	 * An example node.
	 */
	private L317Node node1;

	@Test
	public void hasNextOnEmptySet() {
		final Set<L317Node> nodes = new L317NodeSet<L317Node>(1);
		$it = nodes.iterator();
		Assert.assertFalse($it.hasNext());

	}

	@Test
	public void hasNextOnSetWithANode() {
		final Set<L317Node> nodes = new L317NodeSet<L317Node>(1);
		nodes.add(node1);
		$it = nodes.iterator();
		Assert.assertTrue($it.hasNext());
	}

	@Before
	public void init() {
		node1 = new L317Node();
	}

	@Test
	public void nextOnSetWithANode() {
		final Set<L317Node> nodes = new L317NodeSet<L317Node>(1);
		nodes.add(node1);
		$it = nodes.iterator();
		Assert.assertNotNull($it.next());
	}

	@Test
	public void returnsNullOnNextOnEmptySet() {
		final Set<L317Node> nodes = new L317NodeSet<L317Node>(1);
		$it = nodes.iterator();
		Assert.assertNull($it.next());

	}

}
