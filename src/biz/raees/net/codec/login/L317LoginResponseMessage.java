package biz.raees.net.codec.login;

import biz.raees.model.L317PlayerRights;

/**
 * Represents a login response to be encoded to the RuneScape client.
 *
 * @author raees
 *
 */
final class L317LoginResponseMessage {

	/**
	 * The login opcode.
	 */
	private final L317LoginResponse response;
	/**
	 * The player's rights in-case we need a crown.
	 */
	private final L317PlayerRights rights;
	/**
	 * Is it flagged?
	 */
	private final boolean flagged;

	/**
	 * Encapsulation for the login response.
	 *
	 * @return The login response.
	 */
	final L317LoginResponse getResponse() {
		return response;
	}

	/**
	 * Encapsulation for the player rights.
	 *
	 * @return The player rights.
	 */
	final L317PlayerRights getRights() {
		return rights;
	}

	/**
	 * Encapsulation for the flag.
	 *
	 * @return <code>true</code> or <code>false</code>
	 */
	final boolean isFlagged() {
		return flagged;
	}

	/**
	 * Creates a new login response.
	 *
	 * @param response
	 *            The login response.
	 * @param rights
	 *            The player's rights.
	 * @param flagged
	 *            Is it flagged?
	 */
	L317LoginResponseMessage(L317LoginResponse response, L317PlayerRights rights, boolean flagged) {
		this.response = response;
		this.rights = rights;
		this.flagged = flagged;
	}

}
