package biz.raees.net.codec.login;

/**
 * Represents the login response opcodes.
 *
 * @author raees
 *
 */
enum L317LoginResponse {
	/**
	 * A successful login.
	 */
	NORMAL(2);

	/**
	 * The opcode.
	 */
	private final int opcode;

	/**
	 * Creates a new login response constant.
	 *
	 * @param opcode
	 *            The opcode.
	 */
	private L317LoginResponse(int opcode) {
		this.opcode = opcode;
	}

	/**
	 * Encapsulation for the opcode.
	 *
	 * @return The opcode.
	 */
	final int getOpcode() {
		return opcode;
	}

}
