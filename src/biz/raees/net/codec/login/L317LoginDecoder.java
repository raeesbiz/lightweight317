package biz.raees.net.codec.login;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import com.google.common.base.Preconditions;

import biz.raees.util.L317CipherUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * Decodes bytes sent from the client to a username and password.
 *
 * @author raees
 *
 */
public final class L317LoginDecoder extends ByteToMessageDecoder {

	/**
	 * Holds constants relating to the login protocol.
	 *
	 * @author raees
	 *
	 */
	private static final class LoginDecoderConstants {

		/**
		 * Prevents instantiation of the constants class.
		 */
		private LoginDecoderConstants() {
			throw new AssertionError();
		}

		/**
		 * The RSA modulus
		 */
		private static final BigInteger RSA_MODULUS = new BigInteger(
				"169327564100731580278853347559614532283189712644025436386457696584682294454358593294159554589617573347516175894799698867185598670985319341986336646989539133082176971860162913322726969145657824228946466335110652009360838969659268898184925861820051291616251611031487700123476720909741848538880325032238543293293");

		/**
		 * The RSA exponent.
		 */
		private static final BigInteger RSA_EXPONENT = new BigInteger(
				"96650831101212551002812854197019400089255851970454911337787959684449933181998661182964136565427379705874316918270093767241083282484990554114849341449634235878075151064162919427319767712386720157957331371796700753215102399225518371169658836597118191971764782151915811923733911480131402768780536759699639674753");

	}

	/**
	 * Represents the current state of the decoder.
	 *
	 * @author raees
	 *
	 */
	private enum State {

		/**
		 * Determines whether we want to decode a login request or a file server
		 * request.
		 */
		HANDSHAKE,
		/**
		 * Determines the login opcode.
		 */
		LOGIN_TYPE,
		/**
		 * Sets up the cipher and decodes the username and password.
		 */
		RSA_BLOCK
	}

	/**
	 * The current state.
	 */
	private State state = State.HANDSHAKE;

	/**
	 * A random number generator.
	 */
	private static final SecureRandom RANDOM = new SecureRandom();

	/**
	 * The size of the RSA block.
	 */
	private int rsaBlockSize;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buf1, List<Object> objs) throws Exception {
		switch (state) {
		case HANDSHAKE:
			handshake(ctx, buf1, objs);
			state = State.LOGIN_TYPE;
			break;
		case LOGIN_TYPE:
			loginType(ctx, buf1, objs);
			state = State.RSA_BLOCK;
			break;
		case RSA_BLOCK:
			rsaBlock(ctx, buf1, objs);
			break;

		}

	}

	/**
	 * Decodes the handshake stage.
	 *
	 * @param ctx
	 *            The channel's context.
	 * @param buf1
	 *            The buffer.
	 * @param objs
	 *            The outgoing objects.
	 */
	private final void handshake(ChannelHandlerContext ctx, ByteBuf buf1, List<Object> objs) {
		if (buf1.readableBytes() >= 2) {

			final int opcode = buf1.readUnsignedByte();
			buf1.readUnsignedByte();
			Preconditions.checkState(opcode == 14, opcode);

			final ByteBuf buf = ctx.alloc().buffer(17);
			buf.writeLong(0);
			buf.writeByte(0);
			buf.writeLong(RANDOM.nextLong());
			System.out.println("REACHED END OF HANDSHAKE");
			ctx.writeAndFlush(buf);
		}
	}

	/**
	 * Decodes the stage determining login type.
	 *
	 * @param ctx
	 *            The channel's context.
	 * @param buf1
	 *            The buffer.
	 * @param objs
	 *            The outgoing objects.
	 */
	private final void loginType(ChannelHandlerContext ctx, ByteBuf buf1, List<Object> objs) {
		if (buf1.readableBytes() >= 2) {
			final int loginType = buf1.readUnsignedByte();
			Preconditions.checkState(loginType == 16 || loginType == 18, "loginType != 16 or 18");

			rsaBlockSize = buf1.readUnsignedByte();
			Preconditions.checkState(rsaBlockSize - 40 > 0, "(rsaBlockSize - 40) <= 0");
		}
	}

	/**
	 * Decodes the RSA block.
	 *
	 * @param ctx
	 *            The channel's context.
	 * @param buf1
	 *            The buffer.
	 * @param objs
	 *            The outgoing objects.
	 */
	private final void rsaBlock(ChannelHandlerContext ctx, ByteBuf buf1, List<Object> objs) {
		if (buf1.readableBytes() >= rsaBlockSize) {
			final int magicId = buf1.readUnsignedByte();
			Preconditions.checkState(magicId == 255, "magicId != 255");

			final int clientVersion = buf1.readUnsignedShort();
			Preconditions.checkState(clientVersion == 317, "clientVersion != 317");

			@SuppressWarnings("unused")
			final int memoryVersion = buf1.readUnsignedByte();

			for (int i = 0; i < 9; i++) {
				buf1.readInt();
			}

			final int expectedSize = buf1.readUnsignedByte();
			Preconditions.checkState(expectedSize == rsaBlockSize - 41, "expectedSize != (rsaBlockSize - 41)");

			final byte[] rsaBytes = new byte[rsaBlockSize - 41];
			buf1.readBytes(rsaBytes);

			final ByteBuf rsaBuffer = ctx.alloc().buffer();
			rsaBuffer.writeBytes(new BigInteger(rsaBytes)
					.modPow(LoginDecoderConstants.RSA_EXPONENT, LoginDecoderConstants.RSA_MODULUS).toByteArray());

			final int rsaOpcode = rsaBuffer.readUnsignedByte();
			Preconditions.checkState(rsaOpcode == 10, "rsaOpcode != 10");

			final long clientHalf = rsaBuffer.readLong();
			final long serverHalf = rsaBuffer.readLong();

			final int[] isaacSeed = { (int) (clientHalf >> 32), (int) clientHalf, (int) (serverHalf >> 32),
					(int) serverHalf };

			final L317CipherUtil decryptor = new L317CipherUtil(isaacSeed);
			for (int i = 0; i < isaacSeed.length; i++) {
				isaacSeed[i] += 50;
			}
			final L317CipherUtil encryptor = new L317CipherUtil(isaacSeed);

			@SuppressWarnings("unused")
			final int uid = rsaBuffer.readInt();

			final String username = readRSString(rsaBuffer);
			final String password = readRSString(rsaBuffer);

			rsaBuffer.release();
			System.out.println("REACHED END OF RSA BLOCK");
			objs.add(new Object[] { username, password, encryptor, decryptor, ctx.channel().pipeline() });
		}
	}

	private final String readRSString(ByteBuf buf) {
		byte temp;
		final StringBuilder b = new StringBuilder();
		while ((temp = buf.readByte()) != 10) {
			b.append((char) temp);
		}
		return b.toString();
	}

}
