package biz.raees.net.codec.login;

import com.google.common.base.Preconditions;

import biz.raees.L317Context;
import biz.raees.model.L317Player;
import biz.raees.model.L317PlayerDetails;
import biz.raees.model.L317World;
import biz.raees.net.L317Session;
import io.netty.channel.ChannelHandlerContext;

/**
 * Handles the post login processing.
 *
 * @author raees
 *
 */
public final class L317LoginSession implements L317Session {

	/**
	 * The server's context.
	 */
	private final L317Context l317Ctx;

	/**
	 * Creates a new login session.
	 *
	 * @param l317Ctx
	 *            The server's context.
	 */
	public L317LoginSession(L317Context l317Ctx) {
		this.l317Ctx = l317Ctx;
	}

	@Override
	public void close(ChannelHandlerContext ctx) {
		ctx.channel().disconnect();
	}

	@Override
	public void open(ChannelHandlerContext ctx, Object message) {
		Preconditions.checkArgument(message instanceof Object[]);
		final Object[] loginDetails = (Object[]) message;

		Preconditions.checkArgument(loginDetails[0] instanceof String && loginDetails[1] instanceof String);
		final String username = (String) loginDetails[0];
		final String password = (String) loginDetails[1];

		final L317Player player = new L317Player(l317Ctx, new L317PlayerDetails(username, password));
		final L317World world = l317Ctx.getWorld();
		world.add(player);

		ctx.channel().writeAndFlush(new L317LoginResponseMessage(L317LoginResponse.NORMAL, player.getRights(), false));
	}

}
