package biz.raees.net.codec.login;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Encodes the player credentials to the client.
 *
 * @author raees
 *
 */
public final class L317LoginEncoder extends MessageToByteEncoder<L317LoginResponseMessage> {

	@Override
	protected void encode(ChannelHandlerContext ctx, L317LoginResponseMessage msg, ByteBuf buf1) throws Exception {
		buf1.writeByte(msg.getResponse().getOpcode());

		if (msg.getResponse().equals(L317LoginResponse.NORMAL)) {
			buf1.writeByte(msg.getRights().getId());
			buf1.writeBoolean(msg.isFlagged());
		}

	}

}
