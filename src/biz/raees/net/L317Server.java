package biz.raees.net;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import biz.raees.L317Context;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Holds the networking objects which handle the network management.
 *
 * @author raees
 *
 */
public final class L317Server {

	/**
	 * The port to listen on.
	 */
	private final int port;

	/**
	 * The non blocking event handler.
	 */
	private final EventLoopGroup group = new NioEventLoopGroup();

	/**
	 * Starts the server.
	 */
	private final ServerBootstrap bootstrap = new ServerBootstrap();

	/**
	 * Creates a new server instance.
	 *
	 * @param port
	 *            The port to listen on.
	 */
	public L317Server(int port) {
		this.port = port;
	}

	/**
	 * Binds the server to the port.
	 */
	private final void bind() {
		final SocketAddress address = new InetSocketAddress(port);

		try {
			bootstrap.bind(address).sync();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the networking services.
	 *
	 * @param ctx
	 *            The server's context.
	 */
	public final void init(L317Context ctx) {
		final ChannelInitializer<SocketChannel> service = new L317ChannelInitializer(ctx);

		bootstrap.group(group);
		bootstrap.channel(NioServerSocketChannel.class);
		bootstrap.childHandler(service);

		bind();
	}
}
