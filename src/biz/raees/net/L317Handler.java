package biz.raees.net;

import biz.raees.L317Context;
import biz.raees.net.codec.login.L317LoginSession;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;

/**
 * The channel handler for the server.
 *
 * @author raees
 *
 */
public final class L317Handler extends ChannelInboundHandlerAdapter {

	/**
	 * Represents constants related to the channel handler.
	 *
	 * @author raees
	 *
	 */
	private static final class HandlerConstants {

		/**
		 * Prevents instantiation of the constants class.
		 */
		private HandlerConstants() {
			throw new AssertionError();
		}

		/**
		 * An attribute used to get the session object.
		 */
		private static final AttributeKey<L317Session> SESSION = AttributeKey.valueOf("Session");

		/**
		 * Sent to the console when a channel is registered.
		 */
		private static final String CHANNEL_REGISTERED_MESSAGE = "Channel connected: ";

		/**
		 * Sent to the console when a channel is unregistered.
		 */
		private static final String CHANNEL_UNREGISTERED_MESSAGE = "Channel unregistered: ";
	}

	/**
	 * The server's context.
	 */
	private final L317Context l317Ctx;

	/**
	 * Creates a new channel handler.
	 *
	 * @param l317Ctx
	 *            The server's context.
	 */
	public L317Handler(L317Context l317Ctx) {
		this.l317Ctx = l317Ctx;
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object message) {
		final Channel channel = ctx.channel();
		if (!channel.isOpen()) {
			return;
		}
		if (message instanceof Object[]) {
			final Object[] details = (Object[]) message;
			channel.attr(HandlerConstants.SESSION).set(new L317LoginSession(l317Ctx));
			channel.attr(HandlerConstants.SESSION).get().open(ctx, details);
		} else {
			channel.attr(HandlerConstants.SESSION).get().open(ctx, message);
		}

	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) {
		System.out.println(HandlerConstants.CHANNEL_REGISTERED_MESSAGE + ctx.channel());
	}

	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) {
		final L317Session session = ctx.channel().attr(HandlerConstants.SESSION).get();
		if (session != null) {
			session.close(ctx);
		} else {
			ctx.channel().disconnect();
		}
		System.out.println(HandlerConstants.CHANNEL_UNREGISTERED_MESSAGE + ctx.channel().toString());
	}
}
