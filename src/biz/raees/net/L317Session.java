package biz.raees.net;

import io.netty.channel.ChannelHandlerContext;

/**
 * Represents what to do after login is decoded.
 *
 * @author raees
 *
 */
public interface L317Session {

	/**
	 * The connection is closed.
	 *
	 * @param ctx
	 *            The channel's context.
	 */
	public void close(ChannelHandlerContext ctx);

	/**
	 * The connection is open.
	 *
	 * @param ctx
	 *            The channel's context.
	 * @param message
	 *            The message to read.
	 */
	public void open(ChannelHandlerContext ctx, Object message);
}