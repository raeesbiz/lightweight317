package biz.raees.net;

import biz.raees.L317Context;
import biz.raees.net.codec.login.L317LoginDecoder;
import biz.raees.net.codec.login.L317LoginEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * Initializes the codecs to the channel pipeline.
 *
 * @author raees
 *
 */
public final class L317ChannelInitializer extends ChannelInitializer<SocketChannel> {

	/**
	 * Contains constants related to channel initialization.
	 *
	 * @author raees
	 *
	 */
	private static final class ChannelInitializerConstants {

		/**
		 * Prevents instantiation of the constants class.
		 */
		private ChannelInitializerConstants() {
			throw new AssertionError();
		}

		/**
		 * The unique identifier for the channel adapter.
		 */
		private static final String ADAPTER_IDENTIFIER = "ADAPTER";

		/**
		 * The unique identifier for the channel decoder.
		 */
		private static final String DECODER_IDENTIFIER = "DECODER";

		/**
		 * The unique identifier for the channel encoder.
		 */
		private static final String ENCODER_IDENTIFIER = "ENCODER";
	}

	/**
	 * The server's context.
	 */
	private final L317Context ctx;

	/**
	 * Creates a new channel initializer.
	 *
	 * @param ctx
	 *            The server's context.
	 */
	L317ChannelInitializer(L317Context ctx) {
		this.ctx = ctx;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		final ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast(ChannelInitializerConstants.DECODER_IDENTIFIER, new L317LoginDecoder());
		pipeline.addLast(ChannelInitializerConstants.ENCODER_IDENTIFIER, new L317LoginEncoder());
		pipeline.addLast(ChannelInitializerConstants.ADAPTER_IDENTIFIER, new L317Handler(ctx));
	}

}
