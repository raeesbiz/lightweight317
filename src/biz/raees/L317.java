package biz.raees;

import java.io.IOException;

import biz.raees.net.L317Server;

/**
 * Starts the server
 *
 * @author raees
 *
 */
public final class L317 {

	/**
	 * Represents global server constants
	 *
	 * @author raees
	 *
	 */
	private static final class Constants {

		/**
		 * Prevents instantiation of the constants class.
		 */
		private Constants() {
			throw new AssertionError();
		}

		/**
		 * The port to listen on.
		 */
		private static final int PORT = 43594;

		/**
		 * The maximum number of players.
		 */
		private static final int CAPACITY = 2000;
	}

	/**
	 * The program bootstrap.
	 *
	 * @param args
	 *            The command-line arguments.
	 */
	public static final void main(String[] args) {
		System.out.println("Starting L317...");
		final L317Server server = new L317Server(Constants.PORT);
		L317Context ctx = null;
		try {
			ctx = new L317Context(Constants.CAPACITY);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		server.init(ctx);
		System.out.println("Started L317");
	}

}
