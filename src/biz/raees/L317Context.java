package biz.raees;

import java.io.IOException;

import biz.raees.event.L317EventEngine;
import biz.raees.model.L317World;

/**
 * The core class of the framework which is intended to be passed around to the
 * server.
 *
 * @author raees
 *
 */
public final class L317Context {

	/**
	 * The game world.
	 */
	private final L317World world;

	/**
	 * The event engine.
	 */
	private final L317EventEngine eventEngine;

	/**
	 * Creates a new context object.
	 *
	 * @param capacity
	 *            The capacity of the game world.
	 * @throws IOException
	 *             If the package cannot be found.
	 * @throws InstantiationException
	 *             If the class is not public.
	 * @throws IllegalAccessException
	 *             If the file cannot be accessed.
	 * @throws ClassNotFoundException
	 *             If the class does not exist.
	 */
	public L317Context(int capacity)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		this.world = new L317World(capacity);
		this.eventEngine = new L317EventEngine();
	}

	/**
	 * Encapsulation of the game world.
	 *
	 * @return The world.
	 */
	public final L317World getWorld() {
		return world;
	}

	/**
	 * Encapsulation of the event engine
	 *
	 * @return The event engine.
	 */
	public final L317EventEngine getEventEngine() {
		return eventEngine;
	}

}
