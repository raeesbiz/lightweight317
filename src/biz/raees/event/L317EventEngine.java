package biz.raees.event;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.reflect.ClassPath;

/**
 * Holds all the event encoders and decoders. They are added reflectively when
 * the server is started.
 *
 * @author raees
 *
 */
public final class L317EventEngine {

	/**
	 * Holds constants related to the event engine
	 *
	 * @author raees
	 *
	 */
	private static final class EventEngineConstants {

		/**
		 * Prevents instantiation of the event engine's constant class.
		 */
		private EventEngineConstants() {
			throw new AssertionError();
		}

		/**
		 * Prefix of class file
		 */
		private static final String NAME_PREFIX = "class ";

		/**
		 * The package that holds event decoders
		 */
		private static final String DECODER_PACKAGE = "biz.raees.event.impl.decoders";

		/**
		 * The package that holds event encoders
		 */
		private static final String ENCODER_PACKAGE = "biz.raees.event.impl.encoders";

	}

	/**
	 * The class loader from the current thread.
	 */
	private final ClassLoader loader = Thread.currentThread().getContextClassLoader();

	/**
	 * An unmodifiable map for holding classes to event encoder instances.
	 */
	private final ImmutableMap<Class<?>, L317EventEncoder<?>> encoders;

	/**
	 * An unmodifiable map for holding opcodes to event decoder instances.
	 */
	private final ImmutableMap<Integer, L317EventDecoder<?>> decoders;

	/**
	 * Gets the decoder associated with a particular opcode.
	 *
	 * @param opcode
	 *            The opcode.
	 * @return The decoder.
	 */
	public final L317EventDecoder<?> getDecoder(int opcode) {
		Preconditions.checkArgument(decoders.containsKey(opcode));
		return decoders.get(opcode);
	}

	/**
	 * Gets the encoder associated with a particular class.
	 *
	 * @param clazz
	 *            The class
	 * @return The encoder.
	 */
	public final L317EventEncoder<?> getEncoder(Class<? extends L317Event> clazz) {
		Preconditions.checkArgument(encoders.containsKey(clazz));
		return encoders.get(clazz);
	}

	/**
	 * Creates a new event engine instance.
	 *
	 * @throws IOException
	 *             If the package cannot be found.
	 * @throws InstantiationException
	 *             If the class is not public.
	 * @throws IllegalAccessException
	 *             If the file cannot be accessed.
	 * @throws ClassNotFoundException
	 *             If the class does not exist.
	 */
	public L317EventEngine()
			throws InstantiationException, IllegalAccessException, IOException, ClassNotFoundException {
		this.decoders = ImmutableMap.copyOf(loadDecoders());
		this.encoders = ImmutableMap.copyOf(loadEncoders());
	}

	/**
	 * Loads all the decoders from the decoder package.
	 *
	 * @throws IOException
	 *             If the package cannot be found.
	 * @throws InstantiationException
	 *             If the class is not public.
	 * @throws IllegalAccessException
	 *             If the file cannot be accessed.
	 * @return the opcode-decoder pairings.
	 */
	private final Map<Integer, L317EventDecoder<?>> loadDecoders()
			throws IOException, InstantiationException, IllegalAccessException {
		final ClassPath classPath = ClassPath.from(loader);
		final Map<Integer, L317EventDecoder<?>> temporaryPairings = Maps.newHashMap();

		for (final ClassPath.ClassInfo classInfo : classPath.getTopLevelClasses(EventEngineConstants.DECODER_PACKAGE)) {

			final Class<?> clazz = classInfo.load();

			Preconditions.checkArgument(clazz.getSuperclass().equals(L317EventDecoder.class));
			final L317EventDecoder<?> decoder = (L317EventDecoder<?>) clazz.newInstance();

			Preconditions.checkNotNull(clazz.getAnnotation(L317EventOpcode.class));
			final int opcode = clazz.getAnnotation(L317EventOpcode.class).opcode();

			temporaryPairings.put(opcode, decoder);
		}

		return temporaryPairings;
	}

	/**
	 * Loads all the encoders from the encoder package.
	 *
	 * @throws IOException
	 *             If the package cannot be found.
	 * @throws InstantiationException
	 *             If the class is not public.
	 * @throws IllegalAccessException
	 *             If the file cannot be accessed.
	 * @throws ClassNotFoundException
	 *             If the class does not exist.
	 * @return the class-encoder pairings.
	 */
	private final Map<Class<?>, L317EventEncoder<?>> loadEncoders()
			throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final ClassPath classPath = ClassPath.from(loader);
		final Map<Class<?>, L317EventEncoder<?>> temporaryPairings = Maps.newHashMap();

		for (final ClassPath.ClassInfo classInfo : classPath.getTopLevelClasses(EventEngineConstants.ENCODER_PACKAGE)) {

			final Class<?> clazz = classInfo.load();
			Preconditions.checkArgument(clazz.getSuperclass().equals(L317EventEncoder.class));
			final L317EventEncoder<?> encoder = (L317EventEncoder<?>) clazz.newInstance();

			final Type type = encoder.getType();
			final Class<?> eventClass = Class.forName(getClassName(type));

			temporaryPairings.put(eventClass, encoder);
		}

		return temporaryPairings;
	}

	/**
	 * Gets the class name from a generic type.
	 *
	 * @param type
	 *            The generic type
	 * @return The name.
	 */
	private static final String getClassName(Type type) {
		final String fullName = type.toString();
		if (fullName.startsWith(EventEngineConstants.NAME_PREFIX)) {
			return fullName.substring(EventEngineConstants.NAME_PREFIX.length());
		}
		return fullName;
	}
}
