package biz.raees.event;

import biz.raees.net.L317Packet;

/**
 * Represents a decoder that turns a packet into an event
 *
 * @author raees
 *
 * @param <E>
 *            The event.
 */
public abstract class L317EventDecoder<E extends L317Event> {

	/**
	 * Decodes the received packet into an event
	 *
	 * @param packet
	 *            The packet
	 * @return The event.
	 */
	public abstract E decode(L317Packet packet);

}
