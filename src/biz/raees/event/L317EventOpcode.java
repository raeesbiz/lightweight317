package biz.raees.event;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Represents the opcode of an event
 *
 * @author raees
 *
 */
@Documented
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface L317EventOpcode {

	/**
	 * The opcode
	 *
	 * @return The number of the opcode.
	 */
	int opcode();

}
