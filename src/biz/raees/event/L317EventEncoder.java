package biz.raees.event;

import java.lang.reflect.Type;

import com.google.common.reflect.TypeToken;

import biz.raees.net.L317Packet;

/**
 * Encodes an event into a packet to be sent to the client.
 *
 * @author raees
 *
 * @param <E>
 *            The event
 */
public abstract class L317EventEncoder<E extends L317Event> {

	/**
	 * A type token for the event type.
	 */
	private final TypeToken<E> typeToken = new TypeToken<E>(getClass()) {

		/**
		 * The serial UID.
		 */
		private static final long serialVersionUID = 1L;
	};

	/**
	 * The type of event.
	 */
	private final Type type = typeToken.getType();

	/**
	 * Encodes the event into a packet
	 *
	 * @param event
	 *            The event
	 * @return The packet
	 */
	public abstract L317Packet encode(E event);

	/**
	 * Encapsulation for the type of event
	 *
	 * @return The type.
	 */
	public Type getType() {
		return type;
	}

}
