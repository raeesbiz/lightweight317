package biz.raees.model;

import biz.raees.L317Context;

/**
 * Represents a player in the RuneScape game world.
 *
 * @author raees
 *
 */
public final class L317Player extends L317Node {

	/**
	 * A container for the player's credentials.
	 */
	private final L317PlayerDetails details;
	/**
	 * The server context
	 */
	private final L317Context ctx;
	/**
	 * The permission level of the player.
	 */
	private L317PlayerRights rights = L317PlayerRights.MODERATOR;

	/**
	 * Encapsulation for the player's credentials.
	 *
	 * @return the player details instance.
	 */
	public final L317PlayerDetails getDetails() {
		return details;
	}

	/**
	 * Constructs a new player instance
	 *
	 * @param ctx
	 *            The server context
	 * @param details
	 *            The player's credentials.
	 */
	public L317Player(L317Context ctx, L317PlayerDetails details) {
		this.ctx = ctx;
		this.details = details;
	}

	/**
	 * Encapsulation for the player's permission level.
	 *
	 * @return The player rights.
	 */
	public final L317PlayerRights getRights() {
		return rights;
	}

	/**
	 * Encapsulation for the player's permission level. Sets the player rights.
	 *
	 * @param rights
	 *            The rights to set.
	 */
	public final void setRights(L317PlayerRights rights) {
		this.rights = rights;
	}

	/**
	 * Encapsulation for the server's context.
	 *
	 * @return The context.
	 */
	public final L317Context getCtx() {
		return ctx;
	}

}