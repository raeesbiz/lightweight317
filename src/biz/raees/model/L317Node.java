package biz.raees.model;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/**
 * Represents an entity in the game world such as a player or npc
 *
 * @author raees
 *
 */
public class L317Node {

	/**
	 * Represents the activity level of the entity in the game world
	 *
	 * @author raees
	 *
	 */
	protected enum State {
		/**
		 * The entity is playing the game
		 */
		ACTIVE,
		/**
		 * The entity is sleeping
		 */
		SLEEPING,
		/**
		 * The entity is not playing the game
		 */
		INACTIVE;
	}

	/**
	 * A unique identification number
	 */
	private int id;
	/**
	 * The entity's state
	 */
	private State state = State.SLEEPING;

	/**
	 * Triggered when the entity is active
	 */
	protected void active() {

	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		final L317Node other = (L317Node) obj;
		return Objects.equal(other.id, id) && Objects.equal(other.state, state);
	}

	/**
	 * The unique identification number
	 *
	 * @return the id
	 */
	public final int getId() {
		return id;
	}

	/**
	 * The state of the entity
	 *
	 * @return The state
	 */
	public final State getState() {
		return state;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, state);
	}

	/**
	 * Triggered when the entity is inactive
	 */
	protected void inactive() {

	}

	/**
	 * Sets the entities id
	 *
	 * @param id
	 *            The id to set
	 */
	public final void setId(final int id) {
		this.id = id;
	}

	/**
	 * Updates the entities state
	 *
	 * @param newState
	 *            The new state to set
	 */
	public final void setState(final State newState) {
		Preconditions.checkArgument(newState != State.SLEEPING);
		Preconditions.checkArgument(this.state != newState);

		this.state = newState;

		switch (this.state) {

		case ACTIVE:
			active();
			break;

		case INACTIVE:
			inactive();
			break;

		case SLEEPING:
			sleeping();
			break;
		}
	}

	/**
	 * Triggered when the entity is sleeping
	 */
	protected void sleeping() {

	}

}