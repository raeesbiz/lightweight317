package biz.raees.model;

import java.util.Iterator;

import com.google.common.base.Preconditions;

import biz.raees.model.L317Node.State;

/**
 * An iterator for the NodeSet implementation.
 *
 * @author raees
 *
 * @param <N>
 *            The node.
 */
public final class L317NodeSetIterator<N extends L317Node> implements Iterator<N> {

	/**
	 * A pointer pointing to the previous node.
	 */
	private int previous;

	/**
	 * The current node.
	 */
	private int current = 0;

	/**
	 * The size of the set
	 */
	private final int size;

	/**
	 * The nodes contained in the set
	 */
	private final N[] nodes;

	/**
	 * Creates a new NodeSetIterator.
	 *
	 * @param s
	 *            * The set
	 */
	public L317NodeSetIterator(final N[] nodes) {
		this.size = nodes.length;
		this.nodes = nodes;
	}

	@Override
	public boolean hasNext() {
		return size > current && skip();
	}

	@Override
	public N next() {
		final N node = nodes[current];
		previous = current++;
		return node;
	}

	@Override
	public void remove() {
		Preconditions.checkState(previous != -1);

		skip();

		final N node = nodes[previous];

		final int slot = node.getId() - 1;
		Preconditions.checkArgument(slot != -1);

		node.setState(State.INACTIVE);
		node.setId(-1);
		nodes[slot] = null;
	}

	/**
	 * Traverses the set for nulls
	 *
	 * @return <code>true</code> or <code>false</code>
	 */
	private boolean skip() {
		while (nodes[current] == null) {
			if (++current >= size) {
				return false;
			}
		}
		return true;
	}

}
