package biz.raees.model;

/**
 * Represents the permission level of the player.
 *
 * @author raees
 *
 */
public enum L317PlayerRights {
	/**
	 * A normal player with no crown.
	 */
	PLAYER(0),
	/**
	 * A player with a silver crown.
	 */
	MODERATOR(1),
	/**
	 * A player with a gold crown.
	 */
	ADMINISTRATOR(2);

	/**
	 * Represents the id of the crown.
	 */
	private final int id;

	/**
	 * Creates a new player rights entry.
	 *
	 * @param id
	 *            The id
	 */
	private L317PlayerRights(int id) {
		this.id = id;
	}

	/**
	 * Encapsulation for the id of the crown.
	 *
	 * @return The id.
	 */
	public final int getId() {
		return id;
	}

}
