package biz.raees.model;

import java.util.Set;

import com.google.common.base.Preconditions;

/**
 * Represents a world of players and npcs.
 *
 * @author raees
 *
 */
public final class L317World {

	/**
	 * The set of players
	 */
	private final Set<L317Player> players;

	/**
	 * Creates a new world
	 *
	 * @param playerCapacity
	 *            The number of players in the world
	 */
	public L317World(final int playerCapacity) {
		Preconditions.checkArgument(playerCapacity > 0 && playerCapacity <= 2000);
		players = new L317NodeSet<L317Player>(playerCapacity);
	}

	/**
	 * Adds an entity to the world
	 *
	 * @param node
	 *            The node representing the entity. Be it a player or npc.
	 */
	public final void add(final L317Node node) {
		if (node instanceof L317Player) {
			final L317Player player = (L317Player) node;
			players.add(player);
		}
	}

	/**
	 * Checks if an entity is in the world.
	 *
	 * @param node
	 *            The node we are checking. Be it a player or npc.
	 * @return <code>true</code> or <code>false</code>
	 */
	public final boolean contains(final L317Node node) {
		if (node instanceof L317Player) {
			return players.contains(node);
		} else {
			return false;
		}
	}

	/**
	 * Removes an entity from the world.
	 *
	 * @param node
	 *            The node representing the entity to be removed. Be it player
	 *            or npc.
	 */
	public final void remove(final L317Node node) {
		if (node instanceof L317Player) {
			players.remove(node);
		}

	}

	/**
	 * Gets the number of entities in the world.
	 *
	 * @param clazz
	 *            The type of entity.
	 * @return A integer
	 */
	public final <N extends L317Node> int size(final Class<N> clazz) {
		if (clazz.equals(L317Player.class)) {
			return players.size();
		} else {
			return -1;
		}
	}

}
