package biz.raees.model;

/**
 * Represents the credentials of a player
 *
 * @author raees
 *
 */
public final class L317PlayerDetails {

	/**
	 * The username of the player.
	 */
	private final String username;
	/**
	 * The password of the player
	 */
	private final String password;

	/**
	 * The username represented as a hash code.
	 */
	private final long usernameHash;

	/**
	 * Creates a new player details instance.
	 *
	 * @param username
	 *            The username
	 * @param password
	 *            The password.
	 */
	public L317PlayerDetails(String username, String password) {
		this.username = username;
		this.password = password;
		this.usernameHash = 0;
	}

	/**
	 * Encapsulation for the username.
	 *
	 * @return The username.
	 */
	public final String getUsername() {
		return username;
	}

	/**
	 * Encapsulation for the password.
	 *
	 * @return The password.
	 */
	public final String getPassword() {
		return password;
	}

	/**
	 * Encapsulation for the username-hash.
	 *
	 * @return The hashcode.
	 */
	public final long getUsernameHash() {
		return usernameHash;
	}

}
