package biz.raees.model;

import java.util.AbstractSet;
import java.util.Iterator;

import com.google.common.base.Preconditions;

/**
 * A special type of set for storing Nodes, i.e. players or npcs.
 *
 * @author raees
 *
 * @param <N>
 *            A node
 */
public final class L317NodeSet<N extends L317Node> extends AbstractSet<N> {

	/**
	 * An array of nodes
	 */
	private final N[] nodes;

	@SuppressWarnings("unchecked")
	public L317NodeSet(final int size) {
		this.nodes = (N[]) new L317Node[size];
	}

	@Override
	public boolean add(final N node) {

		Preconditions.checkNotNull(node);
		Preconditions.checkArgument(!contains(node));

		final int slot = findAvailableSlot();

		if (slot != -1) {
			final int id = slot + 1;
			node.setId(id);
			nodes[slot] = node;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Searches the array for a null element and designates it as an available
	 * slot.
	 *
	 * @return The number representing the slot
	 */
	private final int findAvailableSlot() {
		for (int i = 0; i < nodes.length; i++) {
			if (nodes[i] == null) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public Iterator<N> iterator() {
		return new L317NodeSetIterator<N>(nodes);
	}

	@Override
	public int size() {
		return nodes.length;
	}

}
